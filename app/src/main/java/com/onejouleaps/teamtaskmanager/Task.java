package com.onejouleaps.teamtaskmanager;

/**
 * Created by anton on 25.12.14.
 */
public class Task {

    public final String id;
    public final String priority;
    public final String client;
    public final String problem;
    public final String notes;
    public final String startDate;
    public final String endDate;
    public final String user;
    public final String state;

    public String getNotes() {
        return notes;
    }

    public String getPriority() {
        return priority;
    }

    public String getId() {
        return id;
    }

    public String getClient() {
        return client;
    }

    public String getStartDate() {
        return startDate;
    }

    public String getProblem() {
        return problem;
    }

    public String getEndDate() {
        return endDate;
    }

    public String getUser() {
        return user;
    }

    public String getState() {
        return state;
    }

    public Task(String id, String priority, String client, String problem, String startDate, String endDate, String user, String state) {

        this.id = id;
        this.priority = priority;
        this.client = client;
        this.problem = problem;
        this.notes = null;
        this.startDate = startDate;
        this.endDate = endDate;
        this.user = user;
        this.state = state;

    }

    public Task(String id, String priority, String client, String problem, String notes,String startDate, String endDate, String user, String state) {

        this.id = id;
        this.priority = priority;
        this.client = client;
        this.problem = problem;
        this.notes = notes;
        this.startDate = startDate;
        this.endDate = endDate;
        this.user = user;
        this.state = state;

    }

    public Task(String id, String priority, String client, String problem){

        this.id = id;
        this.priority = priority;
        this.client = client;
        this.problem = problem;
        this.notes = null;
        this.startDate = null;
        this.endDate = null;
        this.user = null;
        this.state = null;

    }


}
