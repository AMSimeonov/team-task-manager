package com.onejouleaps.teamtaskmanager;

import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.message.BasicNameValuePair;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by anton on 23.01.15.
 */
public class UrelGenerator {

    public String generateUrl(String startUrl, Integer pageNumber){
        String url = startUrl;

        String stringPageNumber = pageNumber.toString();
        List<NameValuePair> params = new LinkedList<NameValuePair>();
        params.add(new BasicNameValuePair("pageNumber", stringPageNumber));
        String paramString = URLEncodedUtils.format(params, "utf-8");

        url += paramString;
        return url;
    }

    public String generateUrlToFinishTask(String taskId){
        String url = "http://ttm.onejouleapps.com/AndroidApp/FinishTask.php?";

        List<NameValuePair> params = new LinkedList<NameValuePair>();
        params.add(new BasicNameValuePair("taskId", taskId));
        String paramString = URLEncodedUtils.format(params, "utf-8");

        url += paramString;
        return url;
    }
}
