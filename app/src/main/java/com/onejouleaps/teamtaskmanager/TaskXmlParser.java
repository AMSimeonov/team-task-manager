package com.onejouleaps.teamtaskmanager;

import android.util.Xml;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by anton on 25.12.14.
 */
public class TaskXmlParser {



    public List<Task> parse (String xmlInput)throws XmlPullParserException, IOException
    {
        String output = null;
        List<Task> tasks = new ArrayList<Task>();
        String id = null;
        String priority = null;
        String problem = null;
        String notes = null;
        String client = null;
        String startDate = null;
        String endDate = null;
        String user = null;
        String status = null;

        XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
        factory.setNamespaceAware(true);
        XmlPullParser xpp = factory.newPullParser();

        xpp.setInput( new StringReader(xmlInput) );
        int eventType = xpp.getEventType();
        String name = "";
        // xpp.require(XmlPullParser.START_TAG, null, "page");
        while (eventType != XmlPullParser.END_DOCUMENT) {

            if(eventType == XmlPullParser.START_TAG) {

                name = xpp.getName();
                //Cheks on what tag is the parser at this moment.
                if(name.equals("task")){

                    //continue;
                }else if(name.equals("id")){

                    id = readText(xpp);

                }else if(name.equals("priority")){

                    priority = readText(xpp);

                }else if(name.equals("client")){

                    client = readText(xpp);

                }else if (name.equals("problem")) {

                    problem = readText(xpp);

                }else if (name.equals("notes")) {

                    notes = readText(xpp);

                }else if (name.equals("startdate")) {

                    startDate = readText(xpp);

                }else if (name.equals("enddate")) {

                    endDate = readText(xpp);

                }else if (name.equals("user")) {

                    user = readText(xpp);

                }else if (name.equals("status")) {

                    status = readText(xpp);

                }


            } else if(eventType == XmlPullParser.END_TAG) {
                name = xpp.getName();
                if(name.equals("task")){

                    tasks.add(new Task(id, priority, client, problem, notes,startDate, endDate, user, status));

                }
            }

            eventType = xpp.next();

        }

        return tasks;

    }

    private String readText(XmlPullParser parser) throws IOException, XmlPullParserException {
        String result = "";
        if (parser.next() == XmlPullParser.TEXT) {
            result = parser.getText();
            parser.nextTag();
        }
        return result;
    }

//    private List<Task> readPage(XmlPullParser parser) throws XmlPullParserException, IOException {
//        List<Task> tasks = new ArrayList<Task>();
//
//        parser.require(XmlPullParser.START_TAG, ns, "page");
//        while (parser.next() != XmlPullParser.END_TAG) {
//            if (parser.getEventType() != XmlPullParser.START_TAG) {
//                continue;
//            }
//            String name = parser.getName();
//            // Starts by looking for the entry tag
//            if (name.equals("task")) {
//                tasks.add(readTask(parser));
//            } else {
//                skip(parser);
//            }
//        }
//        return tasks;
//    }
//
//    private Task readTask(XmlPullParser parser) throws XmlPullParserException, IOException {
//        parser.require(XmlPullParser.START_TAG, ns, "task");
//        String id = null;
//        String priority = null;
//        String problem = null;
//        String client = null;
//        String startDate = null;
//        String endDate = null;
//        String user = null;
//        String status = null;
//        while (parser.next() != XmlPullParser.END_TAG) {
//            if (parser.getEventType() != XmlPullParser.START_TAG) {
//                continue;
//            }
//            String name = parser.getName();
//            if (name.equals("id")) {
//                id = readId(parser);
//            } else if (name.equals("priority")) {
//                priority = readPriority(parser);
//            } else if (name.equals("client")) {
//                client = readClient(parser);
//            }else if (name.equals("problem")) {
//                problem = readProblem(parser);
//            }else if (name.equals("startdate")) {
//                startDate = readStartDate(parser);
//            }else if (name.equals("enddate")) {
//                endDate = readEndDate(parser);
//            }else if (name.equals("user")) {
//                user = readUser(parser);
//            }else if (name.equals("status")) {
//                status = readStatus(parser);
//            }else {
//                skip(parser);
//            }
//        }
//        return new Task(id, priority, client, problem, startDate, endDate, user, status);
//    }
//
//    private String readText(XmlPullParser parser) throws IOException, XmlPullParserException {
//        String result = "";
//        if (parser.next() == XmlPullParser.TEXT) {
//            result = parser.getText();
//            parser.nextTag();
//        }
//        return result;
//    }
//
//    private String readId(XmlPullParser parser)throws IOException, XmlPullParserException{
//        parser.require(XmlPullParser.START_TAG, ns, "id");
//        String id = readText(parser);
//        parser.require(XmlPullParser.END_TAG, ns, "id");
//        return id;
//    }
//
//    private String readPriority(XmlPullParser parser)throws IOException, XmlPullParserException{
//        parser.require(XmlPullParser.START_TAG, ns, "priority");
//        String priority = readText(parser);
//        parser.require(XmlPullParser.END_TAG, ns, "priority");
//        return priority;
//    }
//
//    private String readClient(XmlPullParser parser)throws IOException, XmlPullParserException{
//        parser.require(XmlPullParser.START_TAG, ns, "client");
//        String client = readText(parser);
//        parser.require(XmlPullParser.END_TAG, ns, "client");
//        return client;
//    }
//
//    private String readProblem(XmlPullParser parser)throws IOException, XmlPullParserException{
//        parser.require(XmlPullParser.START_TAG, ns, "problem");
//        String problem = readText(parser);
//        parser.require(XmlPullParser.END_TAG, ns, "problem");
//        return problem;
//    }
//
//    private String readStartDate(XmlPullParser parser)throws IOException, XmlPullParserException{
//        parser.require(XmlPullParser.START_TAG, ns, "startdate");
//        String startDate = readText(parser);
//        parser.require(XmlPullParser.END_TAG, ns, "startdate");
//        return startDate;
//    }
//
//    private String readEndDate(XmlPullParser parser)throws IOException, XmlPullParserException{
//        parser.require(XmlPullParser.START_TAG, ns, "enddate");
//        String endDate = readText(parser);
//        parser.require(XmlPullParser.END_TAG, ns, "enddate");
//        return endDate;
//    }
//
//    private String readUser(XmlPullParser parser)throws IOException, XmlPullParserException{
//        parser.require(XmlPullParser.START_TAG, ns, "user");
//        String user = readText(parser);
//        parser.require(XmlPullParser.END_TAG, ns, "user");
//        return user;
//    }
//
//    private String readStatus(XmlPullParser parser)throws IOException, XmlPullParserException{
//        parser.require(XmlPullParser.START_TAG, ns, "status");
//        String status = readText(parser);
//        parser.require(XmlPullParser.END_TAG, ns, "status");
//        return status;
//    }
//
//    private void skip(XmlPullParser parser) throws XmlPullParserException, IOException {
//        if (parser.getEventType() != XmlPullParser.START_TAG) {
//            throw new IllegalStateException();
//        }
//        int depth = 1;
//        while (depth != 0) {
//            switch (parser.next()) {
//                case XmlPullParser.END_TAG:
//                    depth--;
//                    break;
//                case XmlPullParser.START_TAG:
//                    depth++;
//                    break;
//            }
//        }
//    }

}
