package com.onejouleaps.teamtaskmanager;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import org.xmlpull.v1.XmlPullParserException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.net.URL;


public class MainActivity extends ActionBarActivity implements AdapterView.OnItemClickListener, TaskAdapter.TaskStatusHandler {
    private final static String TAG = "MainActivity";
    public static final String WIFI = "Wi-Fi";
    public static final String ANY = "Any";
    private static final String URL = "http://ttm.onejouleapps.com/AndroidApp/XMLGenerator.php?pageNumber=1";

    // Whether there is a Wi-Fi connection.
    private static boolean wifiConnected = false;
    // Whether there is a mobile connection.
    private static boolean mobileConnected = false;
    // Whether the display should be refreshed.
    public static boolean refreshDisplay = true;
    public static String sPref = null;

    private ListView lvTasks;
    private Button refresh;
    private Button add;
    private TextView firstPage;
    private TextView leftArrow;
    private TextView rightArrow;
    private TextView currentPageNumber;


    private TaskAdapter adapterTasks;
    private List<Task> tasksList = new ArrayList<Task>();



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        lvTasks = (ListView) findViewById(R.id.xmlList);

        firstPage = (TextView) findViewById(R.id.tvFirst);
        firstPage.setText("first");

        leftArrow = (TextView) findViewById(R.id.tvLeftArrow);
        leftArrow.setText(" - ");

        rightArrow = (TextView) findViewById(R.id.tvRightArrow);
        rightArrow.setText(" + ");

        currentPageNumber = (TextView) findViewById(R.id.tvCurrentPageNumber);
        currentPageNumber.setText(" 1 ");

        add = (Button) findViewById(R.id.bAdd);
        add.setText("Добави");

        refresh = (Button) findViewById(R.id.bRefresh);
        refresh.setText("Обнови");
        refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DownloadXmlTask().execute(URL);
            }
        });

        lvTasks.setOnItemClickListener(this);

    }




    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        loadPage();
    }

    // Uses AsyncTask to download the XML feed from stackoverflow.com.
    public void loadPage() {

        //if((sPref.equals(ANY)) && (wifiConnected || mobileConnected)) {
            new DownloadXmlTask().execute(URL);
            Log.i(TAG, "Is connected");
      //  }
       // else if ((sPref.equals(WIFI)) && (wifiConnected)) {
//            new DownloadXmlTask().execute(URL);
//            Log.i(TAG, "Still connected");
//        } else {
//            // show error
//            Log.i(TAG, "Not connected");
//        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//        Log.i(TAG, "onItemClick fierd.");
        Task task = tasksList.get(position);
        Intent intent = new Intent(this, TaskInfoActivity.class);

        intent.putExtra("id", task.id);
        intent.putExtra("priority", task.priority);
        intent.putExtra("client", task.client);
        intent.putExtra("problem", task.problem);
        intent.putExtra("notes", task.notes);
        intent.putExtra("startDate", task.startDate);
        intent.putExtra("endDate", task.endDate);
        intent.putExtra("user", task.user);
        intent.putExtra("state", task.state);
        startActivity(intent);
    }

    @Override
    public void statusChecked(String taskId) {

        String urlString = "UpdateTask.php?taskId=" + taskId + "&finish=yes";
        new SendUrlRequest().execute(urlString);

    }

    @Override
    public void statusUnchecked(String taskId) {


    }

    // Implementation of AsyncTask used to download XML feed from stackoverflow.com.
    private class DownloadXmlTask extends AsyncTask<String, Void, List<Task>> {
        @Override
        protected List<Task> doInBackground(String... urls) {
            try {
                return loadXmlFromNetwork(urls[0]);
            } catch (IOException e) {
                //return null;
                return new ArrayList<Task>();
            } catch (XmlPullParserException e) {
                //return null;
                return new ArrayList<Task>();
            }
        }

        @Override
        protected void onPostExecute(List<Task> result) {

         tasksList = result;
         try {

         adapterTasks = new TaskAdapter(MainActivity.this, R.layout.task_layout, tasksList);

        }catch (Exception ex){

            Log.i(TAG, "Exception");

        }

        lvTasks.setAdapter(adapterTasks);

        }
    }

    private List<Task> loadXmlFromNetwork(String urlString) throws XmlPullParserException, IOException {

        InputStream stream = null;
        List<Task> tasks = null;
        TaskXmlParser taskXmlParser = null;
        String xmlTasks = null;
        try {

            stream = downloadUrl(urlString);
            xmlTasks = createString(stream);

            taskXmlParser = new TaskXmlParser();
            tasks = taskXmlParser.parse(xmlTasks);

        }catch (Exception ex){

        }
        finally {
            if (stream != null) {
                stream.close();
            }
        }

        return tasks;

    }

    private String createString(InputStream is){
        BufferedReader r = new BufferedReader(new InputStreamReader(is));
        StringBuilder total = new StringBuilder();
        String line;
        try {
            while ((line = r.readLine()) != null) {
                total.append(line);
            }
        }catch (Exception ex){

        }
        return total.toString();
    }

    // Given a string representation of a URL, sets up a connection and gets
// an input stream.
    private InputStream downloadUrl(String urlString) throws IOException {
        URL url;
        url = new URL(urlString);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
//        conn.setReadTimeout(10000 /* milliseconds */);
//        conn.setConnectTimeout(15000 /* milliseconds */);
        conn.setRequestMethod("GET");
        conn.setDoInput(true);
        // Starts the query
        conn.connect();
        return conn.getInputStream();
    }

    private void sendRequest(String urlString)throws IOException{
        URL url;
        url = new URL(urlString);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("GET");
        conn.setDoInput(true);
        conn.connect();
    }

    private class SendUrlRequest extends AsyncTask<String, Void, Void>{

        @Override
        protected Void doInBackground(String... params) {
            try {
                sendRequest(params[0]);
            }catch(IOException ex){

            }
            return null;
        }
    }
}
