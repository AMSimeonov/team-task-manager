package com.onejouleaps.teamtaskmanager;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;


public class TaskInfoActivity extends ActionBarActivity {

    private TextView id;
    private TextView priority;
    private TextView client;
    private TextView problem;
    private TextView notes;
    private TextView startDate;
    private TextView endDate;
    private TextView user;
    private TextView status;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task_info);

        Intent intent = getIntent();
        id = (TextView) findViewById(R.id.tvIdInfo);
        id.setText("Идн: " +  intent.getStringExtra("id").toString());

        priority = (TextView) findViewById(R.id.tvPriorityInfo);
        priority.setText("Приоритет: " + intent.getStringExtra("priority").toString());

        client = (TextView) findViewById(R.id.tvClientInfo);
        client.setText("Клиент: " + intent.getStringExtra("client").toString());

        problem = (TextView) findViewById(R.id.tvProblemInfo);
        problem.setText("Проблем: " + intent.getStringExtra("problem").toString());

        notes = (TextView) findViewById(R.id.tvNotesInfo);
        notes.setText("Бележки: " + intent.getStringExtra("notes").toString());

        startDate = (TextView) findViewById(R.id.tvStartDateInfo);
        startDate.setText(intent.getStringExtra("startDate").toString());

        endDate = (TextView) findViewById(R.id.tvEndDateInfo);
        endDate.setText(intent.getStringExtra("endDate").toString());

        user = (TextView) findViewById(R.id.tvUserInfo);
        user.setText("Техник: " + intent.getStringExtra("user").toString());

        status = (TextView) findViewById(R.id.tvStatusInfo);
        status.setText("Статус: " + intent.getStringExtra("state").toString());
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_task_info, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
