package com.onejouleaps.teamtaskmanager;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by anton on 26.12.14.
 */
public class TaskAdapter extends ArrayAdapter<Task> {

    private Context context;
    private List<Task> tasks = new ArrayList<Task>();


    private TextView client;
    private TextView id;
    private TextView problem;
    private TextView user;
    private TextView startDate;
    private TextView endDate;
    private CheckBox status;
    private TaskStatusHandler taskStatusHandler;

    public TaskAdapter(Context context, int resource, List<Task> tasks) {
        super(context, resource, tasks);
        this.context = context;
        taskStatusHandler = (MainActivity) context;
        this.tasks = tasks;
    }

    public interface TaskStatusHandler{

        void statusChecked(String id);
        void statusUnchecked(String id);

    }

    @Override
    public int getCount() {
        return tasks.size();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        if (row == null) {

            LayoutInflater inflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(R.layout.task_layout, parent, false);

        }

        Task task = (Task) getItem(position);
        client = (TextView) row.findViewById(R.id.tvTaskClient);
        client.setText(task.client);

        id = (TextView) row.findViewById(R.id.tvTaskId);
        id.setText(task.id + " . ");
        final String taskId = task.id;
        problem = (TextView) row.findViewById(R.id.tvTaskProblem);
        problem.setText(task.problem);

        user = (TextView) row.findViewById(R.id.tvTaskUser);
        user.setText(task.user);

        startDate = (TextView) row.findViewById(R.id.tvTaskStartDate);
        startDate.setText(task.startDate);

        endDate = (TextView) row.findViewById(R.id.tvTaskEndDate);
        endDate.setText(task.endDate);

        status = (CheckBox) row.findViewById(R.id.chbTaskStatus);
        boolean isChecked = getStatus(task.state);
        status.setChecked(isChecked);
        status.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked == true){
                    taskStatusHandler.statusChecked(taskId);
                }else{
                    taskStatusHandler.statusUnchecked(taskId);
                }
            }
        });

        return row;
    }

    private boolean getStatus(String status){
        boolean result = false;
        if(status.equals("Yes")){
            result = true;
        }

        return result;
    }
}
